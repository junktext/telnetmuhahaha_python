Project: Telnet Muhahaha! (Using Python 3)
Version: 1.5

Purpose: Shows hows old-school Telnet can still be used to create automated bots
         for many different networking protocols. Remember: RFCs are your friend.
         Currently, the program will create a simple Telnet HTTP bot and create an
         IRC bot to report the details of the detected web server software.

Copyright (C) 2019 - 2020 by William Paul Liggett (junktext.com)

License: GNU Affero General Public License version 3 (AGPLv3)
         https://www.gnu.org/licenses/agpl-3.0.html

Usage:   Just run the main "telnetmuhahaha.py" file and see if it works!
         Potentially, the default site (the one that is uncommented) will not work
         if the remote server is no longer using HTTP/1.1, as they might have
         upgraded it to HTTP/2.0, which this program doesn't interact with
         currently. Feel free to improve this capability and submit a patch!

Demo:    [YouTube: Python + Telnet = Muhahahha!!! (HOPE 2020)](https://www.youtube.com/watch?v=qopipxkbBMk&list=PLIIx_tHQTm2H2qlidZ_wDAdeDhDmNHXH2&index=2&t=0s)
