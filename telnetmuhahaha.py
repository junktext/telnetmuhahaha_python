# Project: Telnet Muhahaha! (Using Python 3)
# Version: 1.5
#
# Purpose: Shows hows old-school Telnet can still be used to create automated bots
#          for many different networking protocols. Remember: RFCs are your friend.
#          Currently, the program will create a simple Telnet HTTP bot and create an
#          IRC bot to report the details of the detected web server software.
#
# Copyright (C) 2019 - 2020 by William Paul Liggett (junktext.com)
#
# License: GNU Affero General Public License version 3 (AGPLv3)
#          https://www.gnu.org/licenses/agpl-3.0.html

# Imports the Telnet library and some time/date modules.
import telnetlib
import time
from datetime import datetime

# Determines which web server to study. Some example results as of July 2020...
host = "www.espn.com"      # Apache/2.4.6 (CentOS) PHP/5.4.16 OpenSSL/1.0.2k-fips
#host = "www.google.com"   # gws
#host = "www.youtube.com"  # YouTube Frontend Proxy
#host = "www.2600.com"     # Apache
#host = "www.airbnb.com"   # nginx

# IRC server to connect to...
irc_server = "irc.underworld.no"  # EFnet seems to be nice to bots.
irc_server_port = 6667

# Which IRC channel to join?
irc_channel = "#TelnetMuhahaha"

# Keep the prefix short!
# For example, EFnet's IRC nick limit is 9 chars and we need 4 chars for the time suffix.
irc_bot_prefix = "Muhah"

# Helps to know when a bot started at what time.
# We'll use this later when connecting to IRC as the bot's name suffix.
current_time = datetime.today()
irc_bot_suffix = current_time.strftime("%H%M") # 24-hour clock notation (e.g., 1600)

# Entire bot's name:
irc_bot_name = irc_bot_prefix + irc_bot_suffix

# Local status message.
print("Attempting to fingerprint: " + host)

# Creates an Telnet bot on HTTP (Port: 80)
tn = telnetlib.Telnet(host, 80)
tn.write(b"GET / HTTP/1.1\n")
tn.write(bytes("HOST: " + host + "\n\n", encoding="ascii"))

# Times out after 15 seconds and then displays the raw HTTP message.
web_data = tn.read_until(b"Connection closed by foreign host.", 15)

# Closes out the HTTP Telnet bot as we've gathered enough details already.
tn.close()

# Converts the Python 'bytes-string' to a regular Unicode string.
web_data_string = web_data.decode("unicode_escape")

# Converts the Unicode string to a List now to have more flexibility.
web_data_list = web_data_string.split("\r\n")

fingerprint_host_server = ""
fingerprint_host_server_bytes = bytes()

for line_of_http_raw in web_data_list:
    # We're looking through the HTTP headers to look for what is reported to us.
    if line_of_http_raw[0:8] == "Server: ":
        fingerprint_host_server = line_of_http_raw

# The web server's software was not reported.
if fingerprint_host_server == "":
    fingerprint_host_server = "Server: UNKNOWN"

# Outputs to the local console (not IRC)
print(fingerprint_host_server)

# General settings for the IRC connection.
print("\n\nConnecting to IRC...")
user_ident = bytes(irc_bot_name + " * * :GuestUser\n", encoding="ascii")
username = bytes(irc_bot_name + "\n", encoding="ascii")
auto_join_channel = bytes(irc_channel, encoding="ascii")

# Connects to the IRC server.
tn = telnetlib.Telnet(irc_server, irc_server_port)

tn.read_until(b"*** No Ident response")
print("Logging onto the IRC server...")
tn.write(b"USER " + user_ident + b"\n")

# Waits 5 seconds.
# This is done every now and then to avoid flooding the IRC server.
time.sleep(5)
tn.write(b"NICK " + username + b"\n")
print("Joining the IRC channel...")
time.sleep(5)
tn.write(b"JOIN " + auto_join_channel + b"\n")
tn.read_until(auto_join_channel + b" :End of /NAMES list.")
time.sleep(5)

# The stuff we care about, such as...
# Website: www.2600.com --> Server: Apache
reported_fingerprint = "Website: " + host + " --> " + fingerprint_host_server

# Converts the string to bytes-string in IRC format with a colon prefix (:)
irc_channel_message = bytes(" :" + reported_fingerprint + "\n", encoding="ascii")

print("Sending the IRC channel message... ")
print(reported_fingerprint)
#tn.write(b"PRIVMSG " + auto_join_channel + b" :Hello, World!\n")  # Basic format.
tn.write(b"PRIVMSG " + auto_join_channel + irc_channel_message)

# Waits 15 seconds before disconnecting.
# Maybe in a future version, the IRC bot could Ping/Pong properly and wait for more commands.
time.sleep(15)

tn.close()